/**
 * Created by yishuangxi on 2016/2/28.
 */
$(function(){
    //侧边栏事件绑定
    (function(){
        //二级菜单点击样式切换
        var $aside2Item = $('.cs-aside-2-item');
        $aside2Item.click(function(){
            $aside2Item.removeClass('cs-aside-2-active');
            $(this).addClass('cs-aside-2-active');
        });
        
        //一级菜单点击收起和展开
        var $aside1Item = $('.cs-aside-1');
        $aside1Item.click(function(){
            $(this).next(".cs-aside-2").toggle();
        });
    })();
});