/**
 * Created by yishuangxi on 2016/2/28.
 */
 
;(function(){
    var vdata = {
        model:"",
        version:"",
        color:"",
        vin:"",
        engine: "" ,
        inprice: "" ,
        jingpin: "" ,
        insurance: "" ,
        state: 1 ,
        productdate: ""
    };
    var vmethods = {
        addItem:addItem
    }
    var v = new Vue({
        el:"#page",
        data:vdata,
        methods:vmethods
    });
    
    function addItem(){
        $.post("/Admin/Stock/addItem", {
            model:vdata.model,
            version:vdata.version,
            color:vdata.color,
            vin:vdata.vin,
            engine: vdata.engine ,
            inprice: vdata.inprice ,
            jingpin: vdata.jingpin ,
            insurance: vdata.insurance ,
            state: vdata.state ,
            productdate: vdata.productdate
        }).done(function(data){
            if(data.code === 1){
                if(!confirm("操作成功！继续添加？")){
                    location = "/Admin/Stock/list";    
                }
            }else{
                alert(data.msg||"操作失败");
            }
        });
    }
})();
