/**
 * Created by yishuangxi on 2016/2/28.
 */
 
;(function(){
    var vdata = {
        toolbar:{
            data:{
                username:"",
                disabled:0,
                page:1
            },
            show:true
        },
        table:{
            data:{
                list:[],
                totalnum:0,
                totalpage:1
            },
            show:true
        },
        edit:{
            data:{}
        },
        add:{
            data:{}
        }
    };
    var vmethods = {
        getItemList:getItemList,
        query:query,
        nextPage:nextPage,
        prevPage:prevPage,
        firstPage:firstPage,
        lastPage:lastPage,
        disableItem:disableItem,
        enableItem:enableItem,
        showEdit:showEdit,
        editItem:editItem,
        enableItem:enableItem,
        disableItem:disableItem
    }
    var v = new Vue({
        el:"#page",
        data:vdata,
        methods:vmethods
    });
    
    vmethods.getItemList();
    
    function getItemList(){
        $.get("/Admin/User/getItemList", {
            username: vdata.toolbar.data.username, 
            disabled: vdata.toolbar.data.disabled,
            page: vdata.toolbar.data.page
        }).done(function(data){
            if(data.code == 1){
                vdata.table.data = data.data;
            }else{
                alert(data.msg||"操作失败");
            }
        }); 
    }
    
    function query(){
        vdata.toolbar.data.page = 1;
        vmethods.getItemList();
    }
    
    function nextPage(){
        vdata.toolbar.data.page += 1;
        vmethods.getItemList();
    }
    
    function prevPage(){
        vdata.toolbar.data.page -= 1;
        vmethods.getItemList();
    }
    
    function firstPage(){
        getItemList(vdata.toolbar.data.username, vdata.toolbar.data.disabled, 1);
    }
    
    function lastPage(){
        getItemList(vdata.toolbar.data.username, vdata.toolbar.data.disabled, vdata.table.data.totalpage);
    }
    
    function disableItem(userid){
        $.post("/Admin/User/disabledItem", {
            userid:userid
        }).done(function(data){
            
        }).error(function(){
            
        });  
    }
    
    function enableItem(userid){
        $.post("/Admin/User/disabledItem", {
            userid:userid
        }).done(function(data){
            
        }).error(function(){
            
        }); 
    }
    
    /****
     * 通过index获取到list中的数据，然后进行编辑操作
     * index是vdata.table.data.list数组的序号
    ****/
    function showEdit(index){
        var $modalEdit = $('#modal-edit');
        vdata.edit.data = vdata.table.data.list[index];
        $modalEdit.modal('show');
    }
    
    function editItem(){
        $.post("/Admin/User/editItem", {
            userid: vdata.edit.data.userid,
            phone: vdata.edit.data.phone,
            disabled: vdata.edit.data.disabled,
            groupid: vdata.edit.data.groupid
        }).done(function(data){
            if(data.code === 1){
                alert("操作成功");
                location.reload();
            }else{
                alert(data.msg||"操作失败");
            }
        });
    }
    
    function disableItem(userid){
        $.post("/Admin/User/disableItem", {
            userid:userid
        }).done(function(data){
            if(data.code === 1){
                alert("操作成功");
                location.reload();
            }else{
                alert(data.msg||"操作失败");
            }
        });
    }
    
    function enableItem(userid){
        $.post("/Admin/User/enableItem", {
            userid:userid
        }).done(function(data){
            if(data.code === 1){
                alert("操作成功");
                location.reload();
            }else{
                alert(data.msg||"操作失败");
            }
        });
    }
    
    window.vdata = vdata;
})();