/**
 * Created by yishuangxi on 2016/2/28.
 */
 
;(function(){
    var vdata = {
        username:"",
        password:"",
        phone:"",
        groupid: 1        
    };
    var vmethods = {
        addItem:addItem
    }
    var v = new Vue({
        el:"#page",
        data:vdata,
        methods:vmethods
    });
    
    function addItem(){
        $.post("/Admin/User/addItem", {
            username:vdata.username,
            phone:vdata.phone,
            groupid:vdata.groupid
        }).done(function(data){
            if(data.code === 1){
                if(!confirm("操作成功！继续添加？")){
                    location = "/Admin/User/list";    
                }
            }else{
                alert(data.msg||"操作失败");
            }
        });
    }
})();
