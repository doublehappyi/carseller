<?php
namespace Common\Controller;
use Think\Controller;
class BaseController extends Controller {
    public function index(){
        $this->display();
    }

    /****
     * @param $err
     */
    protected function ajaxReturnError($msg = "服务器错误")
    {
        $ret = array();
        $ret["code"] = 0;
        $ret["msg"] = $msg;

        $this->ajaxReturn($ret, "JSON");
    }

    /****
     * @param $data
     * @param string $msg
     */
    protected function ajaxReturnSuccess($data = "", $msg = "操作成功")
    {
        $ret = array();
        $ret["code"] = 1;
        $ret["msg"] = $msg;
        $ret["data"] = $data;

        $this->ajaxReturn($ret, "JSON");
    }
    
    /****
     * @return 从session中获取用户名
     */
    protected function getUsername(){
        return $this->getUserInfoFromSession("username");
    }

    /****
     * @return 从session中获取用户名
     */
    protected function getUsernameFromSession(){
        return $this->getUserInfoFromSession("username");
    }

    /**
     * 从Session获取当前登录用户信息
     * @param string $fieldName
     * @return mixed
     */
    protected function getUserInfoFromSession($fieldName = ""){
        $userInfo = session("USER_INFO");
        if (is_null($userInfo)) {
            $this->redirect("/Home/Index/index", null, 0, "");
        } else {
            if (isValidString($fieldName) && array_key_exists($fieldName, $userInfo)) {
                return $userInfo[$fieldName];
            } else {
                return $userInfo;
            }
        }
    }
}