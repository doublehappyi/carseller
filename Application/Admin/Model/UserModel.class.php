<?php
namespace Admin\Model;


class UserModel extends \Common\Model\BaseModel
{
    protected $trueTableName = 'cs_user';

    /****
     * 添加用户，使用该函数之前需要调用一下isUsernameExist
     * @param $username
     * @param $groupid
     * @param $isdelete
     * @return bool|false|int
     */
    public function addItem($username, $phone, $groupid){
        $createtime = getCurrentDatetime();
        $password = '111';
        $disabled = 0;
        $sql = sprintf("insert into cs_user values(NULL, '%s', '%s', '%s', %s, '%s', %s)", $username, $password, $phone, $disabled, $createtime, $groupid);
        try{
            return $this->execute($sql);
        }catch(\Exception $e){
            return false;
        }
    }
    
    
    /****
     * @param $username
     * @param $page
     */
    public function getItemList($username, $disabled, $page){
        $sql = sprintf("select cs_user.*, cs_group.groupname 
                        from cs_user left join cs_group on cs_group.groupid=cs_user.groupid 
                        where cs_user.username like '%%%s%%' and cs_user.disabled=%s order by cs_user.createtime DESC limit %s, %s", 
        $username, $disabled, ($page - 1) * $this::PAGE_SIZE, $this::PAGE_SIZE);
        $totalnumSql = sprintf("select COUNT(username) as totalnum from cs_user where username like '%%%s%%' and disabled=%s", $username, $disabled);
        try{
            $list = $this->query($sql);
            if(count($list) > 0){
                $totalnum = intval($this->query($totalnumSql)[0]["totalnum"]);
            }else{
                $totalnum = 0;
            }
            $totalpage = ceil($totalnum / $this::PAGE_SIZE);
            return array("list" => $list, "totalnum" => $totalnum, "page" => $page, "totalpage" => $totalpage, "pagesize" => $this::PAGE_SIZE);
        }catch(\Exception $e){
            return false;
        }
    }

    /****
     * 禁用用户
     * @param $userid
     * @return bool|false|int
     */
    public function disableItem($userid){
        $sql = sprintf("update cs_user set disabled=1 where userid=%s", $userid);
        try{
            return $this->execute($sql);
        }catch(\Exception $e){
            return false;
        }
    }

    /****启用用户
     * @param $userid
     * @return bool|false|int
     */
    public function enableItem($userid){
        $sql = sprintf("update cs_user set disabled=0 where userid=%s", $userid);
        try{
            return $this->execute($sql);
        }catch(\Exception $e){
            return false;
        }
    }

    /****
     * 编辑用户
     * @param $userid
     * @param $username
     * @param $groupid
     * @param $disabled
     * @return bool|false|int
     */
    public function editItem($userid, $disabled, $phone, $groupid){
        $sql = sprintf("update cs_user set disabled=%s, phone='%s', groupid=%s where userid=%s", $disabled, $phone, $groupid, $userid);
        try{
            return $this->execute($sql);
        }catch(\Exception $e){
            return false;
        }
    }

    /****
     * 修改密码：先验证旧密码
     * @param $userid
     * @param $password
     * @param $newpassword
     * @return UserModel|bool|int 返回值为0说明原密码错误
     */
    public function changePassword($userid, $oldpassword, $newpassword){
        $query_sql = sprintf("select 1 from cs_user where userid=%s and password='%s'", $userid, $oldpassword);
        $change_sql = sprintf("update cs_user set password='%s' where userid=%s", $newpassword, $userid);
        try{
            $user = $this->query($query_sql);
            if(!count($user)){
                return 0;
            }
            return $this->execute($change_sql);
        }catch(\Exception $e){
            return false;
        }
    }
    
    public function resetPassword($userid){
        $sql = sprintf("update cs_user set password='111' where userid=%s", $userid);
    }









    /*****
     * @param $keyword 查询关键字
     * @return bool|mixed
     */
    public function getUserList($keyword, $page){
        $page = ($page - 1) * $this::PAGE_SIZE;
        $sql = sprintf("select username, phone, disabled, createtime, groupid from cs_user where username like '%%%s%%' limit %s, %s", $keyword, $page, $this::PAGE_SIZE);
        try{
            return $this->query($sql);
        }catch(\Exception $e){
            return false;
        }
    }





//
//
//    /****
//     * 获取添加用户必要信息
//     * @return array|bool
//     */
//    public function getUserAddInfo(){
//        $groupModel = new \Admin\Model\GroupModel();
//        $moduleModel = new \Admin\Model\ModuleModel();
//
//        $groupList = $groupModel->getGroupList();
//        if($groupList === false){
//            return false;
//        }
//
//        $moduleList = $moduleModel->getModuleList();
//        if($moduleList === false){
//            return false;
//        }
//        return array("groupList"=>$groupList, "moduleList"=>$moduleList);
//    }
//
//    /****
//     * 获取用户编辑信息：包括组列表及对应组，模块列表及对应权限
//     * @param $username
//     * @return bool
//     */
//    public function getUserEditInfo($username){
//        $groupModel = new \Admin\Model\GroupModel();
//        $moduleModel = new \Admin\Model\ModuleModel();
//
//        $groupList = $groupModel->getGroupList();
//        if($groupList === false){
//            return false;
//        }
//
//        $moduleList = $moduleModel->getModuleListByUsername($username);
//        if($moduleList === false){
//            return false;
//        }
//
//        $user = $this->getUserByName($username);
//        if($user === false){
//            return false;
//        }
//
//        return array("user"=>$user, "groupList"=>$groupList, "moduleList"=>$moduleList);
//    }
//
//    /****
//     * 保存用户信息
//     * @param $username
//     * @param $groupid
//     * @param $isdelete
//     * @param $modules
//     */
//    public function saveUserInfo($username, $groupid, $isdelete, $modules){
//        try{
//            $this->startTrans();
//            $username = $this->saveUserBasicInfo($username, $groupid, $isdelete);
//            if($username === false){
//                $this->rollback();
//                return false;
//            }
//
//            $ret = $this->saveUserModules($username, $modules);
//            if($ret === false){
//                $this->rollback();
//                return false;
//            }
//
//            $this->commit();
//            return "保存成功";
//        }
//        catch(\Exception $e){
//            $this->rollback();
//            return false;
//        }
//    }
//
//    /****
//     * 保存用户模块信息
//     * @param $username
//     * @param $modules
//     * @return bool|false|int
//     */
//    public function saveUserModules($username, $modules){
//        $values = array();
//        //删除原有module信息，新增module信息
//        $ret = $this->deleteUserModulesByUsername($username);
//        if($ret === false){
//            $this->rollback();
//            return false;
//        }
//        for($i=0, $len = count($modules); $i<$len; $i++){
//            array_push($values, sprintf("('%s', %s, %s)", $username, $modules[$i]["moduleid"], $modules[$i]["permission"]));
//        }
//        $sql = "insert into sopr_user_module values " . join(",", $values);
//        try{
//            return $this->execute($sql);
//        }
//        catch (\Exception $e){
//            return false;
//        }
//    }
//
//    /****
//     * 删除指定用户所有模块信息
//     * @param $username
//     * @return bool|false|int
//     */
//    public function deleteUserModulesByUsername($username){
//        $sql = sprintf("delete from sopr_user_module where username='%s'", $username);
//        try{
//            return $this->execute($sql);
//        }
//        catch (\Exception $e){
//            return false;
//        }
//    }
//
//    /****
//     * 保存用户信息：内部判断用户是否已经存在。用户名一旦创建，就不能被修改
//     * @param $username
//     * @param $groupid
//     * @param $isdelete
//     * @return bool
//     */
//    public function saveUserBasicInfo($username, $groupid, $isdelete){
//        $ret = $this->isUsernameExist($username);
//        if($ret === false){
//            return false;
//        }elseif($ret === 1){
//            $this->updateUser($username, $groupid, $isdelete);
//            if($ret === false){
//                return false;
//            }
//        }else{
//            $this->addUser($username, $groupid, $isdelete);
//            if($ret === false){
//                return false;
//            }
//        }
//
//        return $username;
//    }
//
//    /****
//     * 通过username，获取用户详细信息
//     * @param $username
//     * @return bool
//     */
//    public function getUserByName($username){
//        $sql = sprintf("select username, groupid, createtime, isdelete from sopr_user where username='%s'", $username);
//        try {
//            return $this->query($sql)[0];
//        } catch (\Exception $e) {
//            return false;
//        }
//    }
//
//    /****
//     * 更新用户
//     * @param $username
//     * @param $groupid
//     * @param $isdelete
//     * @return bool|false|int
//     */
//    public function updateUser($username, $groupid, $isdelete){
//        $sql = sprintf("update sopr_user set groupid=%s, isdelete=%s where username='%s'", $groupid, $isdelete, $username);
//        try {
//            return $this->execute($sql);
//        } catch (\Exception $e) {
//            return false;
//        }
//    }
//    /****
//     * username是否存在，可以指定groupid查询
//     * 返回值为 1：存在，0：不存在，false：查询失败
//     * @param $username
//     * @param null $groupid
//     * @return bool|int
//     */
//    public function isUsernameExist($username, $groupid=null){
//        if($groupid){
//            $sql = sprintf("select 1 from sopr_user where username='%s' and groupid=%s", $username, $groupid);
//        }else{
//            $sql = sprintf("select 1 from sopr_user where username='%s'", $username);
//        }
//        try {
//            return count($this->query($sql));
//        } catch (\Exception $e) {
//            return false;
//        }
//    }
//
//    /**
//     *获取列表by groupid
//     **/
//    public function getUserListByGroupId($groupid)
//    {
//        $sql = sprintf("select username, createtime, isdelete from sopr_user where groupid=%d", $groupid);
//        try {
//            return $this->query($sql);
//        } catch (\Exception $e) {
//            return false;
//        }
//    }
//
//    /**
//    更新到指定的groupid
//     **/
//    public function updateToGroup($username, $groupid){
//        $sql = sprintf("update sopr_user set groupid=%d where username='%s'", $groupid, $username);
//        try {
//            return $this->execute($sql);
//        } catch (\Exception $e) {
//            return false;
//        }
//    }
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//    /**
//     *新增条目
//     **/
//    public function addItem($username, $groupid, $isdelete)
//    {
//        $createtime = getCurrentDatetime();
//        $sql = "insert into sopr_user values ('%s', %s, '%s', %s)";
//        $sql = sprintf($sql, $username, $groupid, $createtime, $isdelete);
//        try {
//            return $this->execute($sql);
//        } catch (\Exception $e) {
//            return false;
//        }
//    }
//
//    /**
//     *新增条目:带权限的
//     **/
//    public function addItemTrans($username, $groupid, $isdelete, $moduleIdPermResultArr)
//    {
//        //生成user表插入语句
//        $createtime = getCurrentDatetime();
//        $userSql = "insert into sopr_user values ('%s', '%s', '%s', %s)";
//        $userSql = sprintf($userSql, $username, $groupid, $createtime, $isdelete);
//
//        //生成关联表插入语句
//        $userModuleSql = "insert into sopr_user_module values ";
//        $values = array();
//        for ($i = 0, $len = count($moduleIdPermResultArr); $i < $len; $i++) {
//            array_push($values, sprintf("('%s', %s, %s)", $username, $moduleIdPermResultArr[$i][0], $moduleIdPermResultArr[$i][1]));
//        }
//        $userModuleSql = $userModuleSql . join(",", $values);
//
//        try {
//            //事务开始
//            $this->startTrans();
//            if ($this->execute($userSql) === false) {
//                $this->rollback();
//                return false;
//            }
//
//            if ($this->execute($userModuleSql) === false) {
//                $this->rollback();
//                return false;
//            }
//
//            $this->commit();
//            return true;
//        } catch (\Exception $e) {
//            $this->rollback();
//            return false;
//        }
//
//    }
//
//    /**
//     *获取列表
//     **/
//    public function getItemList($username, $isdelete, $page)
//    {
//        $sql = "select username, groupid, (select groupname from sopr_group where groupid=sopr_user.groupid) as groupname, createtime, isdelete
//        from sopr_user
//        where username like '%%%s%%' and isdelete=%s order by createtime ASC  limit %s, %s";
//        $sql = sprintf($sql, $username, $isdelete, ($page - 1) * $this::PAGE_SIZE, $this::PAGE_SIZE);
//
//        $totalnumSql = "select count(username) as totalnum
//        from sopr_user
//        where username like '%%%s%%' and isdelete=%s";
//
//
//        $totalnumSql = sprintf($totalnumSql, $username, $isdelete);
//
//        try {
//            $list = $this->query($sql);
//            $totalnum = $this->query($totalnumSql);
//            $totalnum = intval($totalnum[0]["totalnum"]);
//            $totalpage = ceil($totalnum/$this::PAGE_SIZE);
//            return array("list"=>$list, "totalnum" => $totalnum, "page"=>$page, "totalpage"=>$totalpage, "pagesize"=>$this::PAGE_SIZE);
//        } catch (\Exception $e) {
//            return false;
//        }
//    }
//
//
//    /**
//     *获取列表by groupid
//     **/
//    public function getItemListByGroupId($groupid)
//    {
//        $sql = "select username, groupid, createtime, isdelete from sopr_user where groupid=%d order by createtime ASC ";
//        $sql = sprintf($sql, $groupid);
//
//        try {
//            return $this->query($sql);
//        } catch (\Exception $e) {
//            return false;
//        }
//    }
//
//    /**
//     *获取详情
//     **/
//    public function getItemDetail($username)
//    {
//        $sql = "select username, groupid, createtime, isdelete from sopr_user where username='%s'";
//        $sql = sprintf($sql, $username);
//
//        try {
//            $list = $this->query($sql);
//            return $list[0];
//        } catch (\Exception $e) {
//            return false;
//        }
//    }
//
//    /**
//     *更新条目
//     **/
//    public function editItem($username, $groupid, $isdelete)
//    {
//        $sql = "update sopr_user set groupid='%s', isdelete=%s where username='%s'";
//        $sql = sprintf($sql, $groupid, $isdelete, $username);
//        try {
//            return $this->execute($sql);
//        } catch (\Exception $e) {
//            return false;
//        }
//    }
//
//
//    /**
//     *编辑条目:带权限的
//     **/
//    public function editItemTrans($username, $groupid, $isdelete, $moduleIdPermResultArr)
//    {
//        //生成user表插入语句
//        $userSql = "update sopr_user set groupid='%s', isdelete=%s where username='%s'";
//        $userSql = sprintf($userSql, $groupid, $isdelete, $username);
//
//        //删除语句
//        $delUserModuleSql = sprintf("delete from sopr_user_module where username='%s'", $username);
//
//        //生成sopr_user_user插入语句
//        $insertUserModuleSql = "insert into sopr_user_module values ";
//        $values = array();
//        for ($i = 0, $len = count($moduleIdPermResultArr); $i < $len; $i++) {
//            array_push($values, sprintf("('%s', %s, %s)", $username, $moduleIdPermResultArr[$i][0], $moduleIdPermResultArr[$i][1]));
//        }
//        $insertUserModuleSql = $insertUserModuleSql . join(",", $values);
//
//        try {
//            //事务开始
//            $this->startTrans();
//            //先更新user表
//            if ($this->execute($userSql) === false) {
//                $this->rollback();
//                return false;
//            }
//
//            //再删除旧关系数据
//            if ($this->execute($delUserModuleSql) === false) {
//                $this->rollback();
//                return false;
//            }
//            //再插入新关系数据
//            if ($this->execute($insertUserModuleSql) === false) {
//                $this->rollback();
//                return false;
//            }
//
//            $this->commit();
//            return true;
//        } catch (\Exception $e) {
//            $this->rollback();
//            return false;
//        }
//
//    }
//
//    /**
//     *删除条目
//     **/
//    public function delItem($username)
//    {
//        $sql = "update sopr_user set isdelete=1 where username='%s'";
//        $sql = sprintf($sql, $username);
//        try {
//            return $this->execute($sql);
//        } catch (\Exception $e) {
//            return false;
//        }
//    }
//
//    /**
//     *判断username是否已经存在
//     **/
//    public function isItemExist($username)
//    {
//        $sql = "select 1 from sopr_user where username='%s'";
//        $sql = sprintf($sql, $username);
//        try {
//            return $this->query($sql);
//        } catch (\Exception $e) {
//            return false;
//        }
//    }
}