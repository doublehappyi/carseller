<?php
namespace Admin\Model;


class StockModel extends \Common\Model\BaseModel
{
    protected $trueTableName = 'cs_stock';

    /****
     * 添加库存
     */
    public function addItem($model, $version, $color, $vin, $engine, $inprice, $username, $jingpin, $state, $productdate){
        $indate = getCurrentDatetime();
        $sql = sprintf("insert into cs_stock values(NULL, '%s', '%s', '%s', '%s', '%s', %s, 
                        NULL, '%s', '%s', '%s', NULL, NULL, '%s', '%s', NULL, 0)", 
                        $model, $version, $color, $vin, $engine, $inprice, 
                        $username, $jingpin, $state, $productdate, $indate);
        try{
            return $this->execute($sql);
        }catch(\Exception $e){
            return false;
        }
    }
    
    /****
     * 编辑库存
    */
    public function editItem($stockid, $model, $version, $color, $vin, $engine, $inprice, $outprice, $username, $jingpin, $insurance, $mortgage, $state, $productdate, $outdate, $disabled){
        $sql = sprintf("update cs_stock 
                        set model='%s', version='%s', color='%s', vin='%s', engine='%s', inprice=%s, outprice=%s, 
                        username='%s', jingpin='%s', insurance='%s', mortgage=%s, state=%s, productdate='%s', outdate='%s', disabled=%s
                        where stockid=%s", $model, $version, $color, $vin, $engine, $inprice, $outprice, $username, $jingpin, $insurance, 
                        $mortgage, $state, $productdate, $outdate, $disabled, $stockid);
        try{
            return $this->execute($sql);
        }catch(\Exception $e){
            return false;
        }
    }
    
    
    /****
     * 获取库存列表
     * @param $model
     * @param $version
     * @param $color
     * @param $username
     * @param $
     */
    public function getItemList($model, $version, $color, $username, $disabled, $page){
        $where = "";
        $whereArr = array();
        if($model){
            array_push($whereArr, sprintf("model like '%%%s%%'", $model));
        }
        if($version){
            array_push($whereArr, sprintf("version='%s'", $version));
        }
        if($color){
            array_push($whereArr, sprintf("color='%s'", $color));
        }
        if($username){
            array_push($whereArr, sprintf("username like '%%%s%%'", $username));
        }
        if($disabled){
            array_push($whereArr, sprintf("disabled=%s", $disabled));
        }
        if(count($whereArr) > 0){
            $where = " where " . join(" and ", $whereArr);
        }
        
        $sql = "select * from cs_stock" . $where . sprintf(" limit %s, %s", ($page - 1) * $this::PAGE_SIZE, $this::PAGE_SIZE);
        $totalnumSql = "select COUNT(stockid) as totalnum from cs_stock" . $where;
        
        try{
            $list = $this->query($sql);
            if(count($list) > 0){
                $totalnum = intval($this->query($totalnumSql)[0]["totalnum"]);
            }else{
                $totalnum = 0;
            }
            $totalpage = ceil($totalnum / $this::PAGE_SIZE);
            return array("list" => $list, "totalnum" => $totalnum, "page" => $page, "totalpage" => $totalpage, "pagesize" => $this::PAGE_SIZE);
        }catch(\Exception $e){
            return false;
        }
    }

    /****
     * 下架
     * @param $stockid
     * @return bool|false|int
     */
    public function disableItem($stockid){
        $sql = sprintf("update cs_stock set disabled=1 where stockid=%s", $stockid);
        try{
            return $this->execute($sql);
        }catch(\Exception $e){
            return false;
        }
    }

    /****上架
     * @param $stockid
     * @return bool|false|int
     */
    public function enableItem($stockid){
        $sql = sprintf("update cs_user set disabled=0 where stockid=%s", $stockid);
        try{
            return $this->execute($sql);
        }catch(\Exception $e){
            return false;
        }
    }

    /***
     * 判断username是否已经存在
    **/
    public function isItemExist($username)
    {
        $sql = "select 1 from sopr_user where username='%s'";
        $sql = sprintf($sql, $username);
        try {
            return $this->query($sql);
        } catch (\Exception $e) {
            return false;
        }
    }
}