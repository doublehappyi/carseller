<?php
namespace Admin\Controller;

class IndexController extends \Common\Controller\BaseController
{
    public function index()
    {
        $this->display();
    }
}