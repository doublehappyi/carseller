<?php
namespace Admin\Controller;

class StockController extends \Common\Controller\BaseController
{
    public function index()
    {
        $this->display("list");
    }
    
    public function add(){
        $this->display('add');
    }
    
    public function soldlist(){
        $this->display('soldlist');
    }
    
    
    public function addItem()
    {
        // $stockid = intval(trim(I("stockid")));
        $model = trim(I("model"));
        $version = trim(I("version"));
        $color = trim(I("color"));
        $vin = trim(I("vin"));
        $engine = trim(I("engine"));
        $inprice = intval(trim(I("inprice")));
        $outprice = null;
        $userid = null;
        $jingpin = trim(I("jingpin", ""));
        $insurance = null;
        $mortgage = null;
        $state = intval(trim(I("state", 1)));
        $productdate = trim(I("productdate"));
        $indate = null;
        $outdate = null;

        if (!is_color($color)){
            $this->ajaxReturnError("color参数非法: ".$color);
        }
        
        if (!is_int($inprice)){
            $this->ajaxReturnError("inprice参数非法: ".$color);
        }
        
        // $username = $this->getUsername();
        $username = "yishuangxi";
        $ret = $this->saveItem($model, $version, $color, $vin, $engine, $inprice, $username, $jingpin, $state, $productdate);
        
        if($ret === false){
            $this->ajaxReturnError();
        }
        $this->ajaxReturnSuccess();
    }
    
    public function getItemList()
    {
        $model = trim(I("model", ""));
        $version= trim(I("version", ""));
        $color= trim(I("color", ""));
        $username= trim(I("username", ""));
        $disabled = intval(I("disabled", 0));
        $page = intval(trim(I("page", 1)));
        
        if(!is_page($page)){
            $this->ajaxReturnError("page参数非法： ".$page);
        }
        if(!is_disabled($disabled)){
            $this->ajaxReturnError("disabled参数非法： ".$disabled);
        }
        
        $m = new \Admin\Model\StockModel();
        $data = $m->getItemList($model, $version, $color, $username, $disabled, $page);
        
        if($data === false){
            $this->ajaxReturnError();
        }
        $this->ajaxReturnSuccess($data);
    }
    
    public function editItem(){
        $stockid = intval(trim(I("stockid", "")));
        $model = trim(I("model", ""));
        $version = trim(I("version", ""));
        $color = trim(I("color", ""));
        $vin = trim(I("vin", ""));
        $engine = trim(I("engine", ""));
        $inprice = intval(trim(I("inprice")));
        $outprice = intval(trim(I("outprice", 0)));
        $username = trim(I("username", ""));
        $jingpin = trim(I("jingpin", ""));
        $insurance = trim(I("insurance", ""));
        $mortgage = intval(trim(I("mortgage", "")));
        $state = intval(trim(I("state", 1)));
        $productdate = trim(I("productdate", ""));
        $outdate = trim(I("outdate", ""));
        $disabled = intval(trim(I("disabled", "")));
        
        
        if(!is_stockid($stockid)){
            $this->ajaxReturnError("stockid参数非法： ".$stockid);
        }
        
        if(!is_color($color)){
            $this->ajaxReturnError("color参数非法： ".$color);
        }
        if(!is_price($inprice)){
            $this->ajaxReturnError("inprice参数非法： ".$inprice);
        }
        if(!is_price($outprice)){
            $this->ajaxReturnError("outprice参数非法： ".$outprice);
        }
        if(!is_mortgage($mortgage)){
            $this->ajaxReturnError("mortgage参数非法： ".$mortgage);
        }
        if(!is_state($state)){
            $this->ajaxReturnError("state参数非法： ".$state);
        }
        if(!is_disabled($disabled)){
            $this->ajaxReturnError("disabled参数非法： ".$disabled);
        }
        
        
        $m = new \Admin\Model\StockModel();
        $m->editItem($stockid, $model, $version, $color, $vin, $engine, $inprice, $outprice, $username, $jingpin, $insurance, $mortgage, $state, $productdate, $outdate, $disabled);
        
        if($m === false){
            $this->ajaxReturnError();
        }
        $this->ajaxReturnSuccess();
    }
    
    public function disableItem(){
        $stockid = intval(trim(I("stockid", "")));
        
        if(!is_stockid($stockid)){
            $this->ajaxReturnError("stockid参数非法： ".$stockid);
        }
        $m = new \Admin\Model\StockModel();
        $m->disableItem($stockid);
        
        if($m){
            $this->ajaxReturnSuccess();
        }
        $this->ajaxReturnError();
    }
    
    public function enableItem(){
        $stockid = intval(trim(I("stockid", "")));
        
        if(!is_stockid($stockid)){
            $this->ajaxReturnError("stockid参数非法： ".$stockid);
        }
        $m = new \Admin\Model\StockModel();
        $m->enableItem($stockid);
        
        if($m){
            $this->ajaxReturnSuccess();
        }
        $this->ajaxReturnError();
    }
    
    private function saveItem($model, $version, $color, $vin, $engine, $inprice, $username, $jingpin, $state, $productdate){
        $m = new \Admin\Model\StockModel();
        return $m->addItem($model, $version, $color, $vin, $engine, $inprice, $username, $jingpin, $state, $productdate);
    }
}