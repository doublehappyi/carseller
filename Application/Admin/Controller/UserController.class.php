<?php
namespace Admin\Controller;

class UserController extends \Common\Controller\BaseController
{
    public function index()
    {
        $this->display("list");
    }
    
    public function add(){
        $this->display('add');
    }
    
    public function addItem()
    {
        $username = trim(I("username", ""));
        $phone = trim(I("phone", ""));
        $groupid = intval(I("groupid", 1));

        if (!is_username($username)) {
            $this->ajaxReturnError("username参数非法: ".$username);
        }

        if (!is_phone($phone)) {
            $this->ajaxReturnError("phone参数非法: ".$phone);
        }

        if (!is_groupid($groupid)){
            $this->ajaxReturnError("groupid参数非法: ".$groupid);
        }
        $m = new \Admin\Model\UserModel();
        $ret = $m->addItem($username, $phone, $groupid);
        
        if($ret){
            $this->ajaxReturnSuccess("操作成功");
        }
        $this->ajaxReturnError();
    }
    
    public function getItemList()
    {
        $username = trim(I("username", ""));
        $disabled = intval(I("disabled", 0));
        $page = intval(trim(I("page", 1)));
        
        if(!is_page($page)){
            $this->ajaxReturnError("page参数非法： ".$page);
        }
        if(!is_disabled($disabled)){
            $this->ajaxReturnError("disabled参数非法： ".$disabled);
        }
        
        $m = new \Admin\Model\UserModel();
        $data = $m->getItemList($username, $disabled, $page);
        
        if($data){
            $this->ajaxReturnSuccess($data);
        }
        $this->ajaxReturnError();
        
    }
    
    public function editItem(){
        $userid = intval(trim(I("userid", "")));
        $disabled = intval(trim(I("disabled", 0)));
        $phone = trim(I("phone", ""));
        $groupid = intval(trim(I("groupid", 2)));
        
        if(!is_userid($userid)){
            $this->ajaxReturnError("userid参数非法： ".$userid);
        }
        
        if(!is_disabled($disabled)){
            $this->ajaxReturnError("disabled参数非法： ".$disabled);
        }
        
        if(!is_phone($phone)){
            $this->ajaxReturnError("phone参数非法： ".$phone);
        }
        
        if(!is_groupid($groupid)){
            $this->ajaxReturnError("groupid参数非法： ".$groupid);
        }
        
        $m = new \Admin\Model\UserModel();
        $m->editItem($userid, $disabled, $phone, $groupid);
        
        if($m){
            $this->ajaxReturnSuccess();
        }
        $this->ajaxReturnError();
    }
    
    public function disableItem(){
        $userid = intval(trim(I("userid", "")));
        
        if(!is_userid($userid)){
            $this->ajaxReturnError("userid参数非法： ".$userid);
        }
        $m = new \Admin\Model\UserModel();
        $m->disableItem($userid);
        
        if($m){
            $this->ajaxReturnSuccess();
        }
        $this->ajaxReturnError();
    }
    
    public function enableItem(){
        $userid = intval(trim(I("userid", "")));
        
        if(!is_userid($userid)){
            $this->ajaxReturnError("userid参数非法： ".$userid);
        }
        $m = new \Admin\Model\UserModel();
        $m->enableItem($userid);
        
        if($m){
            $this->ajaxReturnSuccess();
        }
        $this->ajaxReturnError();
    }
}