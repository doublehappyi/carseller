drop database if EXISTS carseller;
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `carseller` DEFAULT CHARACTER SET utf8 COLLATE 'utf8_general_ci';
USE `carseller` ;


-- -----------------------------------------------------
-- Table `cs`.`cs_model`汽车型号静态表
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `carseller`.`cs_model` (
  `model` VARCHAR(32),
  PRIMARY KEY (`model`),
  UNIQUE INDEX `model` (`model` ASC))
ENGINE = InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Table `cs`.`cs_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `carseller`.`cs_user` (
  `userid` INT(10) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(64) UNIQUE NOT NULL,
  `password` VARCHAR(64) DEFAULT '111',
  `phone` VARCHAR(64) UNIQUE NOT NULL,
  `disabled` INT(1) DEFAULT 0,
  `createtime` DATETIME NOT NULL,
  `groupid` INT(10) NOT NULL,
  PRIMARY KEY (`userid`),
  UNIQUE INDEX `userid` (`userid` ASC))
ENGINE = InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Table `cs`.`cs_group` 用户分组静态表
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `carseller`.`cs_group` (
  `groupid` INT(10) NOT NULL AUTO_INCREMENT,
  `groupname` VARCHAR(64) UNIQUE NOT NULL,
  `disabled` INT(1) DEFAULT 0,
  `createtime` DATETIME NOT NULL,
  PRIMARY KEY (`groupid`),
  UNIQUE INDEX `groupid` (`groupid` ASC))
ENGINE = InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Table `cs`.`cs_stock`库存表
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `carseller`.`cs_stock` (
  `stockid` INT(10) NOT NULL AUTO_INCREMENT,
  `model` VARCHAR(64) NOT NULL COMMENT "型号:S6",
  `version` VARCHAR(64) NOT NULL COMMENT "版本:风尚",
  `color` VARCHAR(64) DEFAULT "黑色" COMMENT "颜色:白色，黑色，银色",
  `vin` VARCHAR(64) UNIQUE NOT NULL COMMENT "vin",
  `engine` VARCHAR(64) NOT NULL COMMENT "引擎号",
  `inprice` INT(10) NOT NULL COMMENT "进货价",
  `outprice` INT(10) DEFAULT NULL COMMENT "出货价",
  `username` VARCHAR(64) DEFAULT NULL COMMENT "销售顾问名字",
  `jingpin` VARCHAR(64) DEFAULT NULL COMMENT "精品：膜，脚垫，脚踏板，前后护杠",
  `insurance` VARCHAR(32) DEFAULT NULL COMMENT "保险：大地",
  `mortgage` INT(1) DEFAULT NULL COMMENT "按揭：0 否，1是",
  `state` INT(1) DEFAULT 1 COMMENT "车辆状态：在途1、本店2、二网3、已售4、拆件5",
  `productdate` DATETIME DEFAULT NULL COMMENT "生产日期",
  `indate` DATETIME NOT NULL COMMENT "进店时间",
  `outdate` DATETIME DEFAULT NULL COMMENT "出库时间",
  `disabled` INT(1) DEFAULT 0 COMMENT "上下架状态",
  
  PRIMARY KEY (`stockid`),
  UNIQUE INDEX `stockid` (`stockid` ASC))
ENGINE = InnoDB DEFAULT CHARSET=utf8;



-- 用户分组静态表数据
insert into cs_group values (1, '管理员', 0, '2016-2-28 19:19:19'), (2, '普通用户', 0, '2016-2-28 19:19:19');
-- 车型静态表数据
insert into cs_model values ("S6"), ("S7"), ("S8"), ("S9"), ("S10");
-- 插入用户样本数据
insert into cs_user values (1, "admin","admin", "18888888881", 0,'2016-2-28 19:19:19', 1),
                             (2, "yishuangxi", "111" ,"18888888882", 0, '2016-2-28 19:19:19', 2),
                             (3, "fanfangxiang", "111" ,"18888888883", 0, '2016-2-28 19:19:19', 2),
                             (4, "guest", "111" ,"18888888884", 0, '2016-2-28 19:19:19', 2);
insert into cs_stock values(null, "S6", "风尚", "白色", "vin001", "engine001", "10000000", "20000000", "yishuangxi", "膜，脚踏板", "大地1", 1, 1, "2015-01-01 10:10:10", "2015-08-01 10:10:10", "2016-1-01 10:10:10", 0),
                           (null, "S7", "舒适", "黑色", "vin002", "engine002", "20000000", "40000000", "fanfangxiang", "脚垫，脚踏板", "大地2", 1, 2, "2015-01-01 10:10:10", "2015-08-01 10:10:10", "2016-1-01 10:10:10", 0),
                           (null, "S8", "尊贵", "银色", "vin003", "engine003", "30000000", "60000000", "fanfangxiang", "膜，脚垫，脚踏板", "大地3", 1, 3, "2015-01-01 10:10:10", "2015-08-01 10:10:10", "2016-1-01 10:10:10", 0);
